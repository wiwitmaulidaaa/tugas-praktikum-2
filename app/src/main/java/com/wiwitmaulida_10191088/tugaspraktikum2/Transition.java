package com.wiwitmaulida_10191088.tugaspraktikum2;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.wiwitmaulida_10191088.tugaspraktikum2.R;

public class Transition extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transition);
        Button button = findViewById(R.id.button);
        ImageView imageview = findViewById(R.id.imageView);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean turnOn = false;
                if(!turnOn) {
                    imageview.setImageResource(R.drawable.layouttranson);
                    ((TransitionDrawable)imageview.getDrawable()).startTransition(3000);
                    turnOn = true;
                }else {
                    imageview.setImageResource(R.drawable.layouttransoff);
                    ((TransitionDrawable)imageview.getDrawable()).startTransition(3000);
                    turnOn=false;
                }
            }
        });
    }
}